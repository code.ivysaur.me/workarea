# workarea

![](https://img.shields.io/badge/written%20in-C%2B%2B-blue)

A utility to set the desktop workspace area.

This means that when a window is maximised, it appears only in a subset of the screen space rather than the entire area. It has an unknown impact on Aero Snap.

- Tested on Windows 10 (both 32-bit and 64-bit)
- Compiled with cygwin mingw-w64-i686 package


## Download

- [⬇️ workarea-1.0.src.7z](dist-archive/workarea-1.0.src.7z) *(717B)*
- [⬇️ workarea-1.0.bin.7z](dist-archive/workarea-1.0.bin.7z) *(138.14 KiB)*
